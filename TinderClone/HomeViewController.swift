//
//  HomeViewController.swift
//  TinderClone
//
//  Created by Kerim Çağlar on 16/08/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import Parse

class HomeViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    var data:[String] = ["Seçiniz","Bay","Bayan"]
    var current_username : String?
    
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var genderText: UITextField!
    
    @IBOutlet weak var interestText: UITextField!
    
    var imagePicker = UIImagePickerController()
    var genderPickerView = UIPickerView()
    var interestPickerView = UIPickerView()
    
    var selectedGender:String?
    var selectedInterest:String?
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userName.text = current_username
        profileImage.setRounded()
        
        //PickerView
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
        interestPickerView.delegate = self
        interestPickerView.dataSource = self
        
        genderPickerView.tag = 1
        interestPickerView.tag = 2
        
        genderText.inputView = genderPickerView
        interestText.inputView = interestPickerView
        
        doneButton()
        
        self.errorLabel.isHidden = true
        
        // Profil Bilgilerini Veritabanından Okuma
        if let currentUser = PFUser.current(){
            if let gender = currentUser["gender"]{
                genderText.text = gender as? String
            }
            if let interest = currentUser["interest"]{
                interestText.text = interest as? String
            }
            
            if let photo = currentUser["profilePhoto"] as? PFFile{
                photo.getDataInBackground(block: { (data, error) in
                    if let myImageData = data{
                        if let image = UIImage(data:myImageData){
                            self.profileImage.image = image
                        }
                    }
                })
            }
        }
    }
    
    @IBAction func changeImage(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: "Profil Resmi", message: "Resim seçiniz", preferredStyle: .actionSheet)
        
        let resimGalerisi = UIAlertAction(title: "Resimler", style: .default) { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum)
            {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
                self.imagePicker.allowsEditing = true
                self.present(self.imagePicker, animated:true, completion:nil)
            }
        }
        
        let kamera = UIAlertAction(title: "Kamera", style: .default) { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self.imagePicker.allowsEditing = true
                self.present(self.imagePicker, animated:true, completion:nil)
            }
        }
        
        actionSheet.addAction(resimGalerisi)
        actionSheet.addAction(kamera)
        actionSheet.addAction(UIAlertAction(title:"İptal", style:.cancel, handler:nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    //Profil resmini anlık değiştirir
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage{
            profileImage.image = image
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    //Pickerview Methodları
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 1{
            selectedGender = data[row]
            genderText.text = selectedGender
        }else if pickerView.tag == 2 {
            selectedInterest = data[row]
            interestText.text = selectedInterest
        }else{
            return
        }
    }
    
    //Pickerview a Buton ekleme
    func doneButton(){
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Bitti", style: .plain, target: self, action: #selector(dismissPicker))
        
        toolBar.setItems([doneButton], animated: true)
        genderText.inputAccessoryView = toolBar
        interestText.inputAccessoryView = toolBar
    }
    
    @objc func dismissPicker(){
        view.endEditing(true)
    }
    
    @IBAction func save(_ sender: Any) {
        PFUser.current()?["gender"] = self.selectedGender
        PFUser.current()?["interest"] = self.selectedInterest
        
        if let image = profileImage.image{
            if let imageData = UIImagePNGRepresentation(image){
                PFUser.current()?["profilePhoto"] = PFFile(name: "photo.png", data: imageData)
                
                PFUser.current()?.saveInBackground(block: { (success, error) in
                    if error != nil {
                        if let saveError = error as NSError?{
                            if let errorDetail = saveError.userInfo["error"] as? String{
                                self.errorLabel.isHidden = false
                                self.errorLabel.text = errorDetail
                            }
                        }
                    }else{
                        print("Profil güncelleme işlemi başarıyla tamamlanmıştır")
                        let swipeVC = self.storyboard?.instantiateViewController(withIdentifier: "SwipeVC") as! ViewController
                        self.present(swipeVC, animated: true, completion: nil)
                    }
                })
            }
        }
    }
    
    
}

extension UIImageView {
    
    func setRounded() {
        self.layer.cornerRadius = 25
        self.layer.masksToBounds = true
    }
}
