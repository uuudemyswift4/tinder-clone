//
//  SignUpViewController.swift
//  TinderClone
//
//  Created by Kerim Çağlar on 14/08/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import Parse

class SignUpViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorLabel.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func saveUser(_ sender: Any) {
        
        let user = PFUser()
        user.username = usernameTextField.text //kullanıcı adı
        user.email = emailTextField.text // kullanıcı emaili
        user.password = passwordTextField.text // kullanıcı şifre
        
        user.signUpInBackground { (success, error) in
            if error != nil {
                if let signUpError = error as NSError?{
                    if let errorDetail = signUpError.userInfo["error"] as? String{
                        self.errorLabel.isHidden = false
                        self.errorLabel.text = errorDetail
                    }
                }
            }else{
                let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LogInViewController
                print("Kaydınız başarı ile tamamlanmıştır")
                self.present(loginVC, animated: true, completion: nil)
            }
        }
    }
    
    
    
}
