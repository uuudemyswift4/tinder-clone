//
//  LogInViewController.swift
//  TinderClone
//
//  Created by Kerim Çağlar on 16/08/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import Parse

class LogInViewController: UIViewController {
    
    @IBOutlet weak var userNameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorLabel.isHidden = true // başlangıçta gizli
        // Do any additional setup after loading the view.
    }
    
    @IBAction func logInUser(_ sender: Any) {
        
        let username = userNameTextField.text
        let password = passwordTextField.text
        
        PFUser.logInWithUsername(inBackground: username!, password: password!) { (user, error) in
            
            if error != nil {
                if let logInError = error as NSError?{
                    if let errorDetail = logInError.userInfo["error"] as? String{
                        self.errorLabel.isHidden = false
                        self.errorLabel.text = errorDetail
                    }
                }
            }else{
                if let currentUser = PFUser.current(){
                    let gender = currentUser["gender"]
                    if gender != nil{
                        let swipeVc = self.storyboard?.instantiateViewController(withIdentifier: "SwipeVC") as! ViewController
                        self.present(swipeVc, animated: true, completion: nil)
                    }
                }
                
                let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController
                homeVC.current_username = user?.username // HomeVC ye kullanıcı adını taşıdık
                print("Ana sayfaya hoşgeldiniz")
                self.present(homeVC, animated: true, completion: nil)
            }
        }
    }
    
    
}
