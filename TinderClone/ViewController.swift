//
//  ViewController.swift
//  TinderClone
//
//  Created by Kerim Çağlar on 10/08/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController {
    @IBOutlet weak var pictureView: UIView!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var userProfilePhoto: UIImageView!
    
    var userId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        retrieveUsers()
    }
    
    @IBAction func swipePicture(_ sender: UIPanGestureRecognizer) {
        guard let pictureView = sender.view else {return}
        let shift = sender.translation(in: view)
        pictureView.center = CGPoint(x: view.center.x + shift.x, y: view.center.y + shift.y)
        
        let xDistanceFromCenter = pictureView.center.x - view.center.x
        let rotate = CGAffineTransform(rotationAngle: 0.45)
        let scaleAndRotate = rotate.scaledBy(x: 0.8, y: 0.8)
        pictureView.transform = scaleAndRotate
        
        //Resmin sağa veya sola kaydırıldığını anlama
        if xDistanceFromCenter > 0 {
            likeImageView.alpha = 1
            likeImageView.image = #imageLiteral(resourceName: "up")
        }else{
            likeImageView.alpha = 1
            likeImageView.image = #imageLiteral(resourceName: "down")
        }
        
        likeImageView.alpha = abs(xDistanceFromCenter) / view.center.x
        
        //Resmi merkeze döndürme animasyonu
        if sender.state == UIGestureRecognizerState.ended{
            
            var likeOrUnlike = ""
            
            if pictureView.center.x < 50{
                UIView.animate(withDuration: 0.5, animations: {
                    pictureView.center = CGPoint(x: pictureView.center.x - 200, y: pictureView.center.y)
                    //Burası beğenmediğimiz zaman kullanılıyor
                    likeOrUnlike = "unliked"
                })
                showNewImage(likeOrUnlike: likeOrUnlike)
                animate()
                return
            } else if pictureView.center.x > (view.frame.width - 50){
                UIView.animate(withDuration: 0.5, animations: {
                    pictureView.center = CGPoint(x: pictureView.center.x + 200, y: pictureView.center.y)
                    //Burası beğendiğimiz zaman kullanılıyor
                    likeOrUnlike = "liked"
                })
                showNewImage(likeOrUnlike: likeOrUnlike)
                animate()
                return
            }
            
        }
        
    }
    
    func showNewImage(likeOrUnlike:String){
        if likeOrUnlike != "" && userId != ""
        {
            PFUser.current()?.addUniqueObject(userId, forKey: likeOrUnlike)
            PFUser.current()?.saveInBackground(block: { (success, error) in
                if success{
                    self.retrieveUsers()
                }
            })
        }
    }
    
    func animate(){
        UIView.animate(withDuration: 0.5, animations: {
            self.pictureView.center = self.view.center
            self.likeImageView.alpha = 0
            self.pictureView.transform = CGAffineTransform.identity
        })
    }
    
    @IBAction func logout(_ sender: Any) {
        PFUser.logOut()
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LogInViewController
        self.present(loginVC, animated: true, completion: nil)
    }
    
    
    //Veritabanındaki kullanıcılarımızın resimlerini belli koşula göre çekme
    func retrieveUsers(){
        if let query = PFUser.query(){
            query.whereKey("interest", equalTo: "Bay") // İlgi alanı erkek olanlar
            query.whereKey("gender", equalTo: "Bayan") // Cinsiyeti Bayan Olan
            
            
            //Birkere beğenip yada beğenmediğimi belirttiğim kullanıcıları bir arrayde topluyorum
            var dontShow = [String]()
            
            if let likedUser = PFUser.current()?["liked"] as? [String]{
                dontShow.append(contentsOf: likedUser)
            }
            if let unlikedUser = PFUser.current()?["unliked"] as? [String]{
                dontShow.append(contentsOf: unlikedUser)
            }
            
            query.whereKey("objectId", notContainedIn: dontShow)
            
            query.limit = 3
            
            query.findObjectsInBackground(block: { (objects, error) in
                if let users = objects{
                    for object in users{
                        if let user = object as? PFUser{
                            if let imgFile = user["profilePhoto"] as? PFFile{
                                imgFile.getDataInBackground(block: { (data, error) in
                                    if let imgData = data{
                                        self.userProfilePhoto.image = UIImage(data: imgData)
                                    }
                                    if let objectId = object.objectId{
                                        self.userId = objectId
                                    }
                                })
                            }
                        }
                    }
                    print("query sonucu: \(users)")

                }

            })
            
        }
    }
    
}

