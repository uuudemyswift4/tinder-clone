//
//  MatchTableViewCell.swift
//  TinderClone
//
//  Created by kerimcaglar on 10/09/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit

class MatchTableViewCell: UITableViewCell {

    @IBOutlet weak var matchUserPhoto: UIImageView!
    
    @IBOutlet weak var matchUserName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
