//
//  MatchViewController.swift
//  TinderClone
//
//  Created by kerimcaglar on 10/09/2017.
//  Copyright © 2017 Kerim Çağlar. All rights reserved.
//

import UIKit
import Parse

class MatchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var images = [UIImage]()
    var userIds = [String]()
    var usernames = [String]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let query = PFUser.query(){
            query.whereKey("liked", contains: PFUser.current()?.objectId)
            
            if let likedUser = PFUser.current()?["liked"] as? [String]{
                query.whereKey("objectId", containedIn: likedUser)
            }
            
            query.findObjectsInBackground(block: { (objects, error) in
                if let users = objects{
                    for object in users{
                        if let user = object as? PFUser{
                            if let imgFile = user["profilePhoto"] as? PFFile{
                                imgFile.getDataInBackground(block: { (data, error) in
                                    if let imgData = data{
                                        if let img = UIImage(data: imgData){
                                            self.images.append(img)
                                        }
                                        if let objectId = user.objectId{
                                            self.userIds.append(objectId)
                                        }
                                        if let username = user.username{
                                            self.usernames.append(username)
                                        }
                                        self.tableView.reloadData()
                                    }
                                })
                            }
                        }
                    }
                }
            })
        }
        
    }
    
    //Tableview methodları
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return images.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "matchCell", for: indexPath) as! MatchTableViewCell
        
        cell.matchUserName.text = usernames[indexPath.row]
        cell.matchUserPhoto.image = images[indexPath.row]
        return cell
    }
    

}
